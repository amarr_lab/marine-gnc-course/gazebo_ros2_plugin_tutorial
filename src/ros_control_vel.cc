// #include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/common/common.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/physics/Base.hh>
#include <gazebo/physics/Link.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/World.hh>

#include <gazebo/common/Plugin.hh>

#include <gazebo_ros/node.hpp>

#include <rclcpp/rclcpp.hpp>
#include <ignition/math/Vector3.hh>
// #include <uuv_gazebo_ros_plugins_msgs/msg/float_stamped.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <string>


#include <map>
#include <memory>


namespace gazebo
{
  class RosControlVel : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->model = _parent;

      this->inputCommand = 0.0;

      // Create ROS Node
      myRosNode = gazebo_ros::Node::CreateWithArgs("gazebo_ros2_node");
      // Create Subscription
      mySubVel = myRosNode->create_subscription<geometry_msgs::msg::TwistStamped>(
        "vel_sub", 10,
        std::bind(&RosControlVel::SetReference, this, std::placeholders::_1));
      

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&RosControlVel::OnUpdate, this));
    }

    // Called by the world update start event
    public: void OnUpdate()
    {
      // Apply a small linear velocity to the model.
      this->model->SetLinearVel(ignition::math::Vector3d(this->inputCommand, 0, 0));
    }

    public: void SetReference(
        const geometry_msgs::msg::TwistStamped::SharedPtr _msg)
    {
      if (std::isnan(_msg->twist.linear.x))
      {
        RCLCPP_WARN(myRosNode->get_logger(), "SetReference Vel: Ignoring nan command");
        return;
      }
      this->inputCommand = _msg->twist.linear.x;
      // RCLCPP_WARN(myRosNode->get_logger(), "I heard");
    }

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;
    
    private: double inputCommand;

    protected: gazebo_ros::Node::SharedPtr myRosNode;

    private: rclcpp::Subscription<geometry_msgs::msg::TwistStamped>::SharedPtr mySubVel;

    protected: transport::SubscriberPtr commandSubscriber;

  };  

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(RosControlVel)
}