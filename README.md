# GAZEBO ROS2 PLUGIN TUTORIAL

## Reference
### Gazebo world plugin
#### Tutorials
    - http://gazebosim.org/tutorials/?tut=plugins_hello_world

### Gazebo Model plugin
#### Tutorials
    - https://gazebosim.org/tutorials?tut=plugins_model&cat=write_plugin
#### Reference API
    - http://osrf-distributions.s3.amazonaws.com/gazebo/api/dev/classgazebo_1_1physics_1_1Model.html
